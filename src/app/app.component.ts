import { Component, OnInit } from '@angular/core';
import { CoronaViruService } from './Servicios/CoronaViruService';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'PruebasConsumoApi';

  infoTotalVirus : any[] = [];
  infoPaisVirus : any[] = [];
  PaisesMundo : any[] = [];
  paisSeleccionado ;

  constructor(private virusService : CoronaViruService){}

ngOnInit(){

  this.virusService.getInfoTotalVirus()
     .subscribe(  (dataRecibida : any) => {
      this.infoTotalVirus = dataRecibida.data;
    })

   this.virusService.getAllPaises()
   .subscribe(  (dataRecibida : any) => {
    this.PaisesMundo = dataRecibida;
    console.log(this.PaisesMundo);
  })

}    




onBuscarPorPais(nombre : string){

  this.virusService.getInfoPorPais(nombre)
    .subscribe(  (dataRecibida : any) => {
     this.infoPaisVirus = dataRecibida.data;
     console.log(dataRecibida.data);
   })
}

}
