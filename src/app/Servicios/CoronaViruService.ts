import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class CoronaViruService{


    constructor(private httpClientCorona : HttpClient){}

    getInfoTotalVirus(){
        var headers = new HttpHeaders({
            'rapidapi-key' : '485512b5fdmshcc3f5c11a62b692p1013c6jsndc1c9b5c5984'
        });

      return  this.httpClientCorona.get('https://covid-19-coronavirus-statistics.p.rapidapi.com/v1/total',{headers});
    }

    getInfoPorPais(nombre : string){
        var headers = new HttpHeaders({
            'rapidapi-key' : '485512b5fdmshcc3f5c11a62b692p1013c6jsndc1c9b5c5984'
        });

      return  this.httpClientCorona.get('https://covid-19-coronavirus-statistics.p.rapidapi.com/v1/total?country='+nombre,{headers});
    }

    getAllPaises(){
      return this.httpClientCorona.get('https://restcountries.eu/rest/v2/all');
    }
}